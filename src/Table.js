import React, { Component } from 'react';
import Cell from './Cell';
import Dropdown from 'react-dropdown';

export default class Table extends Component{
    constructor(props){
        super(props);
        this.state = {data: props.difficulty }
        this.baseState = this.state;
        this.resetTableData = this.resetTableData.bind(this);
        this.cells = [];
        
    }
    componentWillReceiveProps(nextProps) {
        console.log("printing state",nextProps.difficulty);
        this.setState({data: nextProps.difficulty});
        this.cells = this.changeCells(nextProps.difficulty)
        console.log("printing the modified cells", this.cells)
    }

    suggestionCalculator(){

    }

    resetTableData(){
        console.log("state reset with props")
        this.setState({data: this.props.difficulty})
    }
    

    cellClickHandler( value, event ){
        event.stopPropagation();
        console.log("cell clicked", value);
    }//End of Cell Click Handler


    reSetHandler(){
        let dfaultData;
        this.state.data.map()
    }


    // prepareDataCell(numbers){
    //     return (numbers.map((number)=>{
    //         let value = number;
    //         let dfault = (number==="-")? true : false;
    //         let suggestions = ['a' , 'b' , 'c'];
    //         return ({
    //             value: value,
    //             dfault: dfault,
    //             suggestions: suggestions
    //         })
    //     }))
    //     // [{value: 4, default: true, suggestion: []  } , {} , {}]
    // }// End of prepareDataCell



    changeCells(numbers){
        console.log("change cells",numbers);
        var keys = Object.keys(numbers);
        let isOpened = false;
        var cells =  keys.map((i)=>{return (
                <Cell className="col 1" key={i} number={numbers[i].value} suggestions= {numbers[i].suggestions} dfault= {numbers[i].dfault} onClick={this.cellClickHandler}/>
        )});

        var nestedArrayCell=[];
        for (var i=0; i< cells.length;i=i+9){
            nestedArrayCell.push(cells.slice(i, i+8)); 
        }
        console.log("nested array cells", nestedArrayCell)
        return nestedArrayCell;
    }//End of changeCells

    render(){
        return (
        <div>
            {this.cells.map((groupCells)=>(<div className = "row">{groupCells}</div>))}
            <button onClick={this.resetTableData}> Restart</button>
            <button onClick={this.suggestionCalculator}> Add suggestion </button>
        </div>
        );
    }
}
