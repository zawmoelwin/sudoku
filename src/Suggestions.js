import React, { Component } from 'react';

export default class Suggestions extends Component{

    render(){
        console.log(this.props);
        return(
            <div onClick={this.props.onClick}>
                {this.props.suggestion}
            </div>
        );
    }
}