import React, { Component } from 'react';
import logo from './logo.svg';
import Table from './Table'
import './App.css';
import data from './data';
import axios from 'axios';
import Dropdown from 'react-dropdown';


var text = `1-58-2----9--764-52--4--819-19--73-6762-83-9-----61-5---76---3-43--2-5-16--3-89--
--5-3--819-285--6-6----4-5---74-283-34976---5--83--49-15--87--2-9----6---26-495-3
29-5----77-----4----4738-129-2--3-648---5--7-5---672--3-9--4--5----8-7---87--51-9
-8--2-----4-5--32--2-3-9-466---9---4---64-5-1134-5-7--36---4--24-723-6-----7--45-
6-873----2-----46-----6482--8---57-19--618--4-31----8-86-2---39-5----1--1--4562--
---6891--8------2915------84-3----5-2----5----9-24-8-1-847--91-5------6--6-41----
-3-5--8-45-42---1---8--9---79-8-61-3-----54---5------78-----7-2---7-46--61-3--5--
-96-4---11---6---45-481-39---795--43-3--8----4-5-23-18-1-63--59-59-7-83---359---7
----754----------8-8-19----3----1-6--------34----6817-2-4---6-39------2-53-2-----
3---------5-7-3--8----28-7-7------43-----------39-41-54--3--8--1---4----968---2--
3-26-9--55--73----------9-----94----------1-9----57-6---85----6--------3-19-82-4-
-2-5----48-5--------48-9-2------5-73-9-----6-25-9------3-6-18--------4-71----4-9-
--7--8------2---6-65--79----7----3-5-83---67-2-1----8----71--38-2---5------4--2--
----------2-65-------18--4--9----6-4-3---57-------------------73------9----------
---------------------------------------------------------------------------------`

class App extends Component {
  constructor(props){
    super(props);
    this.state = {tableData: '' };
    this.handleDifficultyChange= this.handleDifficultyChange.bind(this);
    this.difficultyValue= this.difficultyValue.bind(this);
    this.difficulty={};
  }

  readData(difficulty){
    let array_string = text.split("\n")
    // console.log(typeof (text));
    // console.log(array_string);
    var dataObject={};
    array_string[difficulty].split("").map((char, i)=>{
      if (char === "-"){
        dataObject[i]={
          value: char,
          dfault: false,
          suggestions: []
        } 

      }else{
        dataObject[i]={
          value: char,
          dfault: true,
          suggestions: []
        }
      }
    })

    //readTextFile("file:///C:/your/path/to/file.txt");

    // let testBLOB = new Blob(['../sudoku_puzzles.txt'], { "type" : "text/plain" });
    // FileReader.readAsText(testBLOB);

    //var rawFile = new XMLHttpRequest();
    //rawFile.open("GET", file, false);
    // function readTextFile(file)
    // {
    //     let rawFile = new XMLHttpRequest();
    //     rawFile.open("GET", file, false);
    //     rawFile.onreadystatechange = function ()
    //     {
    //         if(rawFile.readyState === 4)
    //         {
    //             if(rawFile.status === 200 || rawFile.status == 0)
    //             {
    //                 var allText = rawFile.responseText;
    //                 alert(allText);
    //             }
    //         }
    //     }
    //     rawFile.send(null);
    // }

    // let readTxt = readTextFile('../sudoku_puzzles.txt');
    return dataObject;
  }
//this will run after button click
  difficultyValue(){
    console.log("This is from click",this.difficulty)
    console.log("diff input is: ",typeof (this.difficulty));
    this.setState({tableData: this.readData(this.difficulty) });
  }


//this will try to pass the new table data to the Table component
//Still deciding if I want to pass the function to alter the table data state
//making the table component dump


//this will run after user input change in text field
  handleDifficultyChange(e){
     this.difficulty = e.target.value
     console.log("This is from change",this.difficulty)
  }

  render() {
    console.log("Data Passed to Table",this.state.tableData)
    const options = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ];
    return (

      <div className="container">
        <p>choose from 1 to 4</p>
        <input type="text" onChange={this.handleDifficultyChange}></input>
        <button onClick={this.difficultyValue}> Choose Difficulty </button>
        <Table difficulty={this.state.tableData}/>
         <Dropdown disabled={false} options={options} onChange={this._onSelect} value={options[0]} placeholder="Select an option" /> 
      </div>

    );
  }
}

export default App;
