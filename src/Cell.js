import React, { Component } from 'react';
import Suggestions from './Suggestions';
import Dropdown from 'react-dropdown';
import ToggleDisplay from 'react-toggle-display';

export default class Cell extends Component{
    constructor(props){
        super(props);
        this.state = {show: false};
        this.suggestions = this.prepareSuggestion(this.props.suggestions);
        this._onSelect = this._onSelect.bind(this)
    }

    componentWillMount(){
        this.suggestions = this.prepareSuggestion(this.props.suggestions);
    }

    componentWillReceiveProps(props){
        this.suggestions = this.prepareSuggestion(props.suggestions);
    }

    prepareSuggestion(suggestions){
        suggestions = [1 , 2, 3, 4]
        return  (suggestions.map((suggestion)=>{
            return { label: suggestion, value: suggestion};
        })
    )
    }
    _onSelect(option){
        console.log("You selected label", option.label)
        console.log("You selected value", option.value)
        this.setState({
        show: !this.state.show
        }); 
    }

    handleClick() {
        if (!this.props.dfault){
            this.setState({
            show: !this.state.show
            });
        }
    }

    cellClicked(){
        console.log("cell click handler", "parent is clicked")
    }

    render(){
        var disabled = false;
        console.log ("suggestion log", this.suggestions);
        return(
            <div>
                <div onClick = {this.handleClick.bind(this)}> 
                    {this.props.number}
                </div>
                <ToggleDisplay show={this.state.show}>
                    <Dropdown disabled={false} options = {this.suggestions} value = {this.suggestions[0]} onChange={this._onSelect} placeholder="Select an option"/> 
                </ToggleDisplay>
                
            </div>
            
        );
    }
}